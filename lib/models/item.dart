const keyId = 'id';
const keyName = 'pizzaName';
const keyDescription = 'description';
const keyPrice = 'price';
const keyImage = 'imageUrl';

class Item {
  late int? id;
  late String itemName;
  late String? description;
  late double? price;
  late String? imageUrl;

  Item.createDefault() {
    id = null;
    itemName = '';
    description = '';
    price = null;
    imageUrl = '';
  }

  Item({
    required this.id,
    required this.itemName,
    required this.description,
    required this.price,
    required this.imageUrl
  });

  Item.fromJson(Map<String, dynamic> json) {
    this.id = json['id'] ?? 0;
    this.itemName = json[keyName] ?? '';
    this.description = json[keyDescription] ?? '';
    this.price = json[keyPrice] ?? 0.0;
    this.imageUrl = json[keyImage] ?? '';
  }

  set pizzaName(String pizzaName) {
      this.itemName = pizzaName;
  }


  Map<String, dynamic> toJson() {
    return {
      keyId: id,
      keyName: itemName,
      keyDescription: description,
      keyPrice: price,
      keyImage: imageUrl,
    };
  }
}