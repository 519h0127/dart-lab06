import 'dart:convert';
import 'dart:io';
import 'package:my_food_list/models/item.dart';
import 'package:http/http.dart' as http;

class HttpService{
  final String server = 'rocky.mocklab.io';
  final String path = 'pizzalist';
  final String postPath = 'pizza';
  final String putPath = 'pizza';
  final String deletePath = 'pizza';

  Future<List<Item>> getPizzaList() async {
    Uri url = Uri.https(server, path);
    http.Response result = await http.get(url);
    if (result.statusCode == HttpStatus.ok) {
      final jsonResponse = json.decode(result.body);
      //provide a type argument to the map method to avoid type error
      List<Item> items = jsonResponse.map<Item>((i) => Item.fromJson(i)).toList();
      print(items);
      return items;
    } else {
      return [];
    }
  }

  Future<http.Response> deleteItem(int id) async {
    Uri url = Uri.https(server, deletePath);
    return http.delete(
        url,
        body: id
    );

  }

  Future<http.Response> postItem(Item item) {
    String post = json.encode(item.toJson());
    Uri url = Uri.https(server, postPath);
    print(post);

    return http.post(
      url,
      body: post,
    );
  }

  Future<http.Response> putPizza(Item item) async {
    String put = json.encode(item.toJson());
    Uri url = Uri.https(server, putPath);

    return http.put(
      url,
      body: put,
    );
  }

}