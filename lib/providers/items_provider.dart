import 'package:flutter/cupertino.dart';

import '../models/item.dart';
import '../services/http_services.dart';

class ItemsProvider with ChangeNotifier{
  late List<Item> _items;
  HttpService helper = HttpService();

  List<Item> get getItems => _items;

  Future<void> refreshItems() async {
    print('Use HttpHelper to get list of pizzas...');
    _items = await helper.getPizzaList();
    notifyListeners();
  }


}