import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:my_food_list/screens/home_screen.dart';
import 'package:provider/provider.dart';

import '../providers/items_provider.dart';

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    loadData() async{
      ItemsProvider _itemProvider =  Provider.of<ItemsProvider>(context,listen:false);
      await _itemProvider.refreshItems();
    }
    loadData();

    return MultiProvider(
      providers: [
        ChangeNotifierProvider(create: (context)=>ItemsProvider())
      ],

      child: MaterialApp(
        title: "",
        theme: ThemeData(
            primarySwatch: Colors.deepOrange,
            visualDensity:VisualDensity.adaptivePlatformDensity
        ),
        home: HomeScreen(),
      ),
    );
  }
}
