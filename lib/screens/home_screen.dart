import 'dart:math';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:my_food_list/screens/add_edit_item_screen.dart';
import 'package:my_food_list/services/http_services.dart';
import 'package:provider/provider.dart';

import '../models/item.dart';
import '../providers/items_provider.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({Key? key}) : super(key: key);
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  late List<String> item_list = [];

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    // addPizzas();
    // loadData();
  }

  loadData() async{
    ItemsProvider _itemProvider =  Provider.of<ItemsProvider>(context,listen:false);
    await _itemProvider.refreshItems();
  }

  addPizzas(){
    item_list.add("Lobster");
    item_list.add("Crabs");
    item_list.add("Beefs");
    item_list.add("Porks");
    item_list.add("Pinapple");
  }

  Widget showList(){
    return ListView.builder(
        padding: EdgeInsets.all(10),
        itemCount: item_list.length,
        itemBuilder: (context,index){
          return RowItem(context,index);
        }
    );
  }

  Widget RowItem(context,index){
    return Dismissible(
        key: UniqueKey(),
        onDismissed: (direction){
          var pizza = item_list[index];
          showSnackBar(context,pizza,index);
          removeItem(index);
        },
        background: deleteBgItem(),
        child: Card(
          child: ListTile(
            title: Text(item_list[index]),
          ),
        )
    );
  }

  Widget deleteBgItem(){
    return Container(
      alignment: Alignment.centerRight,
      padding: EdgeInsets.only(right: 20),
      color: Colors.red,
      child: Icon(Icons.delete,color: Colors.white,),
    );
  }

  showSnackBar(context,item,index){
    Scaffold.of(context).showSnackBar(
        SnackBar(
            content: Text('${item['pizzaName']} deleted'),
          action: SnackBarAction(
            label: 'UNDO ',
            onPressed: (){
              undoDelete(index, item);
            },
          ),
        )
    );
  }

  undoDelete(index, item){
    setState(() {
      item_list.insert(index, item);
    });
  }

  removeItem(index){
    setState(() {
      item_list.removeAt(index);
    });
  }

  @override
  Widget build(BuildContext context) {
    final List<Item> items =Provider.of<ItemsProvider>(context).getItems;
    print("Items ${items[0].toJson()}");


    return Scaffold(
      appBar: AppBar(
        title: Text("MyFoodList"),
      ),
      body: Container(
        child: ListView.builder(
            padding: EdgeInsets.all(10),
            itemCount: items.length,
            itemBuilder: (context,index){
              var item = items[index].toJson();
              return Dismissible(
                  key: UniqueKey(),
                  onDismissed: (direction){
                    showSnackBar(context,item,index);
                    HttpService httpHelper = HttpService();
                    httpHelper.deleteItem(item['id']);
                  },
                  background: deleteBgItem(),
                  child: Card(
                    child: ListTile(
                      title: Text(item['pizzaName']),
                      subtitle: Text("${item['description'] } \$ ${item['price']}"),
                      trailing: Text("\$ ${item['price']}"),
                      onTap: () {
                        // _editPizza(context, itemData);
                        Navigator.push(context, MaterialPageRoute(
                            builder: (context)=> AddEditItemScreen(item: items[index])
                        ));
                      },
                    ),
                  )
              );
            }
        ),
      ),
      floatingActionButton: FloatingActionButton(
          child: Icon(Icons.add),
          onPressed: () async{
              Item defaultItem = Item.createDefault();
              //Add pizza
              Navigator.push(context, MaterialPageRoute(
                  builder: (context)=> AddEditItemScreen(item: defaultItem)
              ));
          },
      ),
    );
  }
}
