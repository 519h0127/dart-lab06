import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:my_food_list/services/http_services.dart';

import '../models/item.dart';

class AddEditItemScreen extends StatefulWidget {
  Item item;
  AddEditItemScreen({Key? key, required this.item}) : super(key: key);

  @override
  _AddEditItemScreenState createState() => _AddEditItemScreenState();
}

class _AddEditItemScreenState extends State<AddEditItemScreen>{
  final formKey = GlobalKey<FormState>();

  var nameController = TextEditingController();
  var descriptionController = TextEditingController();
  var priceController = TextEditingController();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    var item = widget.item;
    nameController.text = item.itemName;
    descriptionController.text = item.description ?? '';
    priceController.text = (item.price != null) ? item.price.toString() : '';
  }


  @override
  Widget build(BuildContext context) {
    var item = widget.item;

    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
          onPressed: () {
            Navigator.of(context).pushReplacementNamed('/');
          },
          icon: Icon(Icons.arrow_back),
        ),
        title: Text(item.id == null ? 'New pizza' : 'Update pizza'),
      ),
      body: Container(
        child: Form(
            key: formKey,
            child: Column(
              children: [
                _fieldName(),
                _fieldDescription(),
                _fieldPrize(),
                _buttonSave(),
              ],
            ),
        ),
      ),
    );
  }

  Widget _fieldName() {
    return TextFormField(
      controller: nameController,
      decoration: InputDecoration(
        labelText: 'Name',
      ),
    );
  }
  Widget _fieldDescription() {
    return TextFormField(
      controller: descriptionController,
      keyboardType: TextInputType.multiline,
      decoration: InputDecoration(
          labelText: 'Description',
          helperMaxLines: 3
      ),
    );
  }

  Widget _fieldPrize() {
    return TextFormField(
      controller: priceController,
      keyboardType: TextInputType.number,
      decoration: InputDecoration(
          labelText: 'Price'
      ),
    );
  }
  Widget _buttonSave() {
    return ElevatedButton(
        onPressed: () {
          _savePizza();
        },
        child: Text('Save')
    );

  }

  void _savePizza() async {
    var httpService = HttpService();
    var item = widget.item;

    item.pizzaName = nameController.text;
    item.description = descriptionController.text;
    item.price = priceController.text.isNotEmpty ? double.parse(priceController.text) : null;

    var jsonItem = item.toJson();
    print(jsonItem);

    var response;
    if (item.id == null) {
      // New
      response = await httpService.postItem(item);

      if (response.statusCode == 201) {
        print('Save the pizza successfully.');
        print(await httpService.getPizzaList());
        print('Response message= ${response.body}');
      } else {
        print('Could not save the pizza.');
        print('Response message= ${response.body}');
      }
    } else {
      // Update
      response = await httpService.putPizza(item);

      if (response.statusCode == 200) {
        print('Update the pizza successfully.');
        print('Response message= ${response.body}');
      } else {
        print('Could not update the pizza.');
        print('Response message= ${response.body}');
      }
    }

  }

}
